#include <stdio.h>

struct AccessRecord {       //init a new struct
    int customerID;         //elements of struct
    char domain[256];
    char timestamp[21]; 
    };

void EditRecord (struct AccessRecord rc[], int i) {                     //function for adding new/editing current record, passing in member of struct, array rc and index i
    printf("Please insert the customer ID:\n");                         
    scanf("%d", &rc[i].customerID);                                     //placing customer id into member rc of struct at [index] and item customer id
    printf("Please insert the domain:\n");
    scanf("%256s", rc[i].domain);                                       //placing customer id into member rc of struct at [index] and item domain
    printf("Please insert the time stamp as yyyy-mm-dd hh:mm:ss:\n");
    scanf(" %[^\n]s", rc[i].timestamp);                                 //placing customer id into member rc of struct at [index] and item timestamp
}

void PrintRecord (struct AccessRecord rc[], int i) {                                                        //function for printing all current records
    for (int c = 0; c < i; c++) {
        printf("%d | %s customer %d accessed %s\n", c+1, rc[c].timestamp, rc[c].customerID, rc[c].domain);  //for loop print data in a formatted way
    }
}

int main() {
    
    int menu = '?';                                                             //option variable for the menu                                                      
    int i = 0;                                                                  //index at 0
    struct AccessRecord rc[100];                                                //new struct member rc, capacity 100

    while(1) {                                                                  // forever
	
		printf("\n1) Add a new record\n2) Display all records\n0) Quit\n\n>");  // Print the main menu
		scanf("%d", &menu);            		                                    // Grab the option from the user
				
		if(menu == 1) {                                                         //menu option for entering new record  
            printf("Entering new record\n");                        
            EditRecord(rc,i);                                                   //calling function and passing struct member and index
            i++;                                                                //increase index by 1
            continue;
        }

		if(menu == 2) {                                                         //menu option for display record  
            PrintRecord(rc,i);                                                  //calling function and passing struct member and index
            printf("\nNumber of record - %d\n", i);                             //printing index as the total number of records
            continue;
		}

		if(menu == 0) {                                                         //menu option for quitting
			break;                                                              //break if selected
		}
	}
    return 0;
}