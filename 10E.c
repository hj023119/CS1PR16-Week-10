#include <stdio.h>
#include <string.h>

int x = 0;

struct AccessRecord {       //init a new struct
    int customerID;         //elements of struct
    char domain[256];
    char timestamp[21]; 
    };

struct AccessRecord rc[1000];

void EditRecord (struct AccessRecord rc[], int i) {                     //function for adding new/editing current record, passing in member of struct, array rc and index i
    printf("Please insert the customer ID:\n");                         
    scanf("%d", &rc[i].customerID);                                     //placing customer id into member rc of struct at [index] and item customer id
    printf("Please insert the domain:\n");
    scanf("%256s", rc[i].domain);                                       //placing customer id into member rc of struct at [index] and item domain
    printf("Please insert the time stamp as yyyy-mm-dd hh:mm:ss:\n");
    scanf(" %[^\n]s", rc[i].timestamp);                                 //placing customer id into member rc of struct at [index] and item timestamp

    FILE *fd;                                   //appoint file pointer
    fd = fopen("./example_data.ipb","a");       //opening the file from current directory (linux)
    fprintf(fd, "\n%d|%s|%s", rc[i].customerID, rc[i].domain, rc[i].timestamp);
    fclose(fd);
}

int Readfile(struct AccessRecord rc[]) {        //function to read file
    int i = 0; char line[299]; 
    const char s[2] = "|";
    
    FILE *fd;                                   //appoint file pointer
    fd = fopen("./example_data.ipb","r");       //opening the file from current directory (linux)

    while (fgets(line, sizeof(line), fd)) {     //while all the lines are being read
        
        char *token;                            //variable for seprated data
        
        token = strtok(line, s);                //to seprate data from 'line'
        rc[i].customerID = strtol(token, NULL, 10);     //writing first part into customerid (converting char to int)
        token = strtok(NULL, s);                

        strcpy (rc[i].domain , token);          //writing second part into domains
        token = strtok(NULL, s);
            
        strcpy (rc[i].timestamp , token);       //writing third part into time stamp
        token = strtok(NULL, s);

        i++;                                    //line conunter addition
    }
    fclose(fd);                                 //closing file
    
    return(i);
}

void PrintRecord (struct AccessRecord rc[],int i) {                                                        //function for printing all current records
    for (int c = 0; c <= i-1; c++) {
        printf("%d. customer %d accessed %s at %s", c+1, rc[c].customerID, rc[c].domain, rc[c].timestamp); //printing everything in correct format
    }
}

void Search (struct AccessRecord rc[], int x, int c) {      //function for searching
    for(int i = 0;i < x;i++)
    {
        if (rc[i].customerID == c)                  //if matched
        {
            printf("%d. customer %d accessed %s at %s", i, rc[i].customerID, rc[i].domain, rc[i].timestamp); //printing everything in correct format
        }
    }
    printf("No more matching record\n");
}

int main() {
      
    int menu = '?';                                                             //option variable for the menu   

    while(1) {                                                                  // forever
	
		printf("\n1) Add a new record\n2) Display all records\n3) Read records from file\n0) Quit\n\n>");
		scanf("%d", &menu);            		                                    // Grab the option from the user
				
		if(menu == 1) {                                                         //menu option for entering new record  
            printf("Entering new record\n");                        
            EditRecord(rc,x);                                                   //calling function and passing struct member and index
            x++;                                                                //increase index by 1
            continue;
        }

		if(menu == 2) {                                                         //menu option for display record  
            PrintRecord(rc,x);                                                  //calling function and passing struct member and index
            printf("\nTotal number of record - %d\n", x);                             //printing index as the total number of records
            continue;
		}

        if(menu == 3) {                                                         //menu option to read file
            int f = Readfile(rc);                                               //calling function and passing in struct
            x = x + f;                                                          //counting the total record number
            printf("\n File read, number of record - %d\n", f);                 //printing the number
            continue;
		}

        if(menu == 4) {                                                         //menu option to read file
           int cid;                                                         //customer id to search for
           printf("Enter a customer number to search:\n");          
           scanf("%d", &cid);                                           //getting customer id from user
           Search(rc,x,cid);                                            //passing into function
		}

		if(menu == 0) {                                                         //menu option for quitting
			break;                                                              //break if selected
		}
	}
    return 0;
}
