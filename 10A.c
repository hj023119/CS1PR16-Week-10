#include <stdio.h>

struct AccessRecord {       //init a new struct
    int customerID;         //elements of struct
    char domain[256];
    char timestamp[21]; 
    };

int main() {
    
    struct AccessRecord r00001;
    printf("Please insert the customer ID:\n");
    scanf("%d", &r00001.customerID);                                                            //customer id for member r00001 is placed in struct
    printf("Please insert the domain:\n");
    scanf("%255s", r00001.domain);                                                              //domain accessed member r00001 is placed in struct
    printf("Please insert the time stamp as yyyy-mm-dd hh:mm:ss:\n");
    scanf(" %[^\n]s", r00001.timestamp);                                                        //timestamp of member r00001 is placed in struct

    printf ("\n%s User %d visited %s \n", r00001.timestamp, r00001.customerID, r00001.domain);  //printing all data in a formatted way

    return 0;
} 